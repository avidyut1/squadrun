/*
  TODO
  Tie braker
 */
const PORT = 7000;
const io = require('socket.io').listen(PORT);
var mysql = require('mysql');
var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "ashvid71",
  database: "squadrun_dev"
});
connection.connect(function(err){
  if(!err){
    console.log("Database connected");
  }
  else{
    console.log("Database not connected");
  }
});
var sockets = []
var users = []
var paired = []
var userclick = []
var questions = []
io.sockets.on('connection', function(socket){
	//subscriber redis client for this socket
  sockets.push(socket);
  socket.on('message', function(msg) {
    if(msg.hasOwnProperty("user_id")){
      if(users.indexOf(parseInt(msg["user_id"])) >= 0){
        return;
      }
      users.push(msg["user_id"]);
      paired.push(-1);
    }
  });
  socket.on("userfree", function (msg){
    var uid = parseInt(msg["user_id"]);
    var ind = users.indexOf(uid);
    if(ind >= 0){
      paired[ind] = -1
      userclick[ind] = [];
      // console.log(userclick)
      // console.log(paired)
    }
  });
  socket.on("game", function(msg){
    // console.log(users)
    var fuid = parseInt(msg["fu"]);
    var suid = parseInt(msg["su"]);
    var indexf = users.indexOf(fuid);
    var indexs = users.indexOf(suid);
    if(questions[indexf] && questions[indexf].length > 0){
      sockets[indexf].send("open -1 "+questions[indexf][0]);
      sockets[indexs].send("open -1 "+questions[indexf][0]);
      return;
    }
    //fetch random questions
    connection.query("SELECT num_rounds, num_questions from game_configs ORDER BY id DESC LIMIT 1", function (error, rows){
      if(!error){
        var numquestions = rows[0].num_rounds * rows[0].num_questions;
        connection.query("SELECT id from questions ORDER BY RAND() LIMIT ?",[numquestions], function (error, rows, fields){
          if(!error){
            // questions[fuid] = rows
            questions[indexf] = []
            questions[indexs] = []
            for (var i = rows.length - 1; i >= 0; i--) {
              questions[indexf].push(rows[i]["id"])
              questions[indexs].push(rows[i]["id"])
            };
            console.log("questions")
            console.log(questions[indexf])
            console.log(questions[indexs])
            userclick[indexf] = []
            userclick[indexs] = []
            sockets[indexf].send("open "+msg["gid"]+" "+questions[indexf][0]);
            sockets[indexs].send("open "+msg["gid"]+" "+questions[indexf][0]);
          }
          else{
            console.log(error);
          }
        });
      }
      else{
        console.log(error);
      }
    });
  });
  socket.on("answer", function (msg){
    var uid = parseInt(msg["user_id"])
    var indexuser = users.indexOf(uid)
    var questionid = msg["question"]
    var time = msg["time"];
    var indexans = -1;
    console.log(uid," sent answer");
    if(userclick[indexuser].length == 0){
      console.log("in")
      userclick[indexuser] = [];
      userclick[indexuser].push([questionid, msg["answer"], time]);
      indexans = 0;
    }
    else{
      var found = false;
      for (var i = userclick[indexuser].length - 1; i >= 0; i--) {
        if(userclick[indexuser][i][0] == questionid && userclick[indexuser][i][1] == msg["answer"]){
          found = true;
          indexans = i;
          break;
        }
      };
      if(!found){
        userclick[indexuser].push([questionid, msg["answer"], time]);
        indexans = userclick[indexuser].length - 1;
      }
    }
    var puserind = paired[indexuser];
    // console.log(paired)
    var match = false;
    if(userclick[puserind].length != 0){
      for (var i = userclick[puserind].length - 1; i >= 0; i--) {
        if(userclick[puserind][i][0] == questionid){
          if(userclick[puserind][i][1] === userclick[indexuser][indexans][1]){
            var indq = questions[indexuser].indexOf(questionid);
            sockets[indexuser].send("open "+(questions[indexuser][indq + 1]));
            sockets[puserind].send("open "+(questions[indexuser][indq + 1]));
            match = true;
            break;
          }   
        }
      };
    }
    if(!match){
      sockets[indexuser].send("mismatch");
      sockets[puserind].send("mismatch");
    }
    console.log(userclick)
  });
  socket.on("gameend", function (msg){
    console.log("gameend")
    var uid = parseInt(msg["fu"]);
    var index = users.indexOf(uid);
    var pind = paired[index]
    if(paired[index] == -1){
      return;
    }
    //conect to mysql and add score
    var roundwinu = 0;
    var roundwinpu = 0;
    var scoreu = 0;
    var scorepu = 0;
    console.log(index)
    console.log(pind)
    connection.query("SELECT * from game_configs ORDER BY id DESC LIMIT 1", function (error, rows){
      if(!error){
        var res = rows[0]
        var roundwinpoint = res["round_win_points"] 
        var roundloosepoint = res["round_loose_points"] 
        var gamewinpoint = res["game_win_points"] 
        var gameloosepoint = res["game_loose_points"] 
        var round = res["num_rounds"]
        var question = res["num_questions"]
        var indq = 0;
        console.log(round)
        console.log(question)
        for(var rc = 1; rc <= round; rc++){
          var sumtimeu = 0;
          var sumtimepu = 0;
          for(var qc = 1; qc <= question; qc++){
            var questionid = questions[index][indq];
            console.log(questionid)
            for (var i = userclick[index].length - 1; i >= 0; i--) {
              if(userclick[index][i][0] == questionid){
                sumtimeu += userclick[index][i][2];
                break;
              }
            };
            for (var i = userclick[pind].length - 1; i >= 0; i--) {
              if(userclick[pind][i][0] == questionid){
                sumtimepu += userclick[pind][i][2];
                break;
              }
            };
            console.log(questions)
          }
          if(sumtimeu < sumtimepu){
            roundwinu++;
            scoreu+=roundwinpoint;
            scorepu-=roundloosepoint;
          }
          else{
            roundwinpu++;
            scorepu+=roundwinpoint;
            scoreu-=roundloosepoint;
          }
        }
        if(roundwinu > roundwinpu){
          scoreu+=gamewinpoint;
          scorepu-=gameloosepoint;
        }
        else{
          scorepu+=gamewinpoint;
          scoreu-=gameloosepoint;
        }
        console.log("score first"+scorepu)
        console.log("scoere sec"+scoreu)
        if(scorepu == scoreu){
          connection.query("SELECT id from questions ORDER BY RAND() LIMIT 1", function (error, rows, fields){
            var questionid = rows[0]["id"];
            questions[index].add(questionid);
            questions[pind].add(questionid);
            sockets[index].send("open "+questionid);
            sockets[pind].send("open "+questionid);
            return;
          });
        }
        connection.query("UPDATE users SET score = ?  WHERE id = ?",[scoreu, users[index]], function (error, rows, fields){
          if(!error){
            console.log("Updated");
            connection.query("UPDATE users SET score = ? WHERE id = ?", [scorepu, users[pind]], function (error, rows, fields){
              if(!error){
                console.log("Updated");
                paired[pind] = -1;
                paired[index] = -1;
                userclick[pind] = [];
                userclick[index] = [];
                sockets[index].send("close game");
                sockets[pind].send("close game");
              }
              else{
                console.log(error)
              }
            });
          }
          else{
            console.log(error)
          }
        });
      }
      else{
        console.log(error);
      }  
    });
  });
  socket.on("pair", function(msg){
    var uid = parseInt(msg["user_id"])
    // console.log(uid)
    var ind = users.indexOf(uid)
    // console.log(paired)
    if(paired[ind] != -1){
      return
    }
    if(users.length < 2){
      socket.send("Waiting for a player");
    }
    else{
      for (var i = 0; i < users.length; i++) {
        if(users[i] == parseInt(msg["user_id"]))
          continue;
        else{
          if(paired[i] != -1){
            continue;
          }
          var index = users.indexOf(msg["user_id"]);
          paired[index] = i;
          paired[i] = index;
          socket.send("Paired with "+users[i]);
          sockets[i].send("Playing game with "+users[index]);
          break;
        }
      };
    }
  });
  socket.on('disconnect', function() {
    var index = sockets.indexOf(socket);
    users.splice(index, 1);
    if(paired[index] != -1 && sockets[paired[index]] != null)
      sockets[paired[index]].send("Opponent disconnected");
    sockets.splice(index, 1);
    paired.splice(index, 1);
  });
});