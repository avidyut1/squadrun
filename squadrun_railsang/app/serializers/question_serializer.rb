class QuestionSerializer < ActiveModel::Serializer
  attributes :id, :title, :img_url
  def attributes
  	data = super
  	data[:answer] = Question.find(object.id).answers
  	data
  end
end
