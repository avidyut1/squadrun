class UserSerializer < ActiveModel::Serializer
  attributes :id, :score, :name
end
