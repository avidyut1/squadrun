class QuestionsController < ApplicationController
  def index
    @questions = Question.all
    render json: @questions, each_serializer: QuestionSerializer
  end
  def show
    @ind = params[:id]
    @question = Question.find(@ind)
    render json: @question, serialize: QuestionSerializer, :root => false 
  end
  def userquestions
		@user_id = session[:user_id]  
		@user = User.find(@user_id)
		@questions = @user.questions
		render json: @questions, each_serializer: QuestionSerializer
  end
  def create
  	title = params[:title]
  	img_url = params[:img_url]
  	answers = params[:answers]
  	@question = Question.new
  	@question.title = title
  	@question.img_url = img_url
  	@question.user_id = session[:user_id]
  	for answer in answers
  		@ans = Answer.new
  		@ans.img_url = answer
  		@ans.save
  		@question.answers << @ans
  	end
  	if @question.save
  		render json: {result: "success"}
  	else
  		render json: {result: "error"}
  	end
  end

  def update
    question_id = params[:id]
    @question = Question.find(question_id)
    @question.answers.each do |answer|
      answer.destroy
    end
    @question.title = params[:title]
    @question.img_url = params[:img_url]
    @panswers = params[:answer]
    if @panswers
      @panswers.each do |answer|
        @ans = Answer.new
        @ans.img_url = answer[:img_url]
        @ans.question_id = @question.id
        @ans.save
      end 
    end
    if @question.save
      render json: {result: "success"}
    else
      render json: {result: "error"}
    end
  end

  def destroy
    @question_id = params[:id]
    @question = Question.find(@question_id)
    @question.answers.each do |answer| 
      answer.destroy
    end
    if @question.destroy
      render json: {result: "success"}
    else
      render json: {result: "error"}
    end
  end
end
