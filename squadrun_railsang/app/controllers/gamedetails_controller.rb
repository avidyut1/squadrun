class GamedetailsController < ApplicationController
  def create
  	gameid = params[:gameid]
  	questionid = params[:questionid]
  	answerid = params[:answerid]
  	@gd = GameDetail.new
  	@gd.game_id = gameid
  	@gd.question_id = questionid
  	@gd.answer_id = answerid
  	if @gd.save
  		render json: {result: "success"}
  	else
  		render json: {result: "error"}
  	end
  end
end
