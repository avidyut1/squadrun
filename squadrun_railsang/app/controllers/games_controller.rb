class GamesController < ApplicationController
	def create
		@game = Game.new
		@game.first_user_id = params[:first_user]
		@game.second_user_id = params[:second_user]
		if @game.save
			render json: {result: "success", gameid: @game.id}
		else
			render json: {result: "error"}
		end
	end

end
