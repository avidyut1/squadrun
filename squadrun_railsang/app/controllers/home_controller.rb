class HomeController < ApplicationController
	def index
		
	end
	def conf
		if request.post?
			gamewp = params[:gamewp]
			gamelp = params[:gamelp]
			roundwp = params[:roundwp]
			roundlp = params[:roundlp]
			questions = params[:questions]
			rounds = params[:rounds]
			@c = GameConfig.new
			@c.num_rounds = rounds
			@c.num_questions = questions
			@c.game_win_points = gamewp
			@c.game_loose_points = gamelp
			@c.round_win_points = roundwp
			@c.round_loose_points = roundlp
			if @c.save
				render json: {result: "success"}
			else
				render json: {result: "error"}
			end
			return
		end
		@conf = GameConfig.last
		render json: @conf
	end
	def hof
		@user = User.all.order(score: :desc)
		render json: @user, :each_serializer => UserSerializer, :root => false
	end
	def signup
		name = params[:name]
		email = params[:email]
		password = params[:password]
		@user = User.new
		@user.name = name
		@user.email = email
		@user.password = password
		if @user.save
			render json: {result: "success", user_id: @user_id, role: @user.role}
		else
			render json: {result: "Save error"}
		end
	end
	def login
		email = params[:email]
		password = params[:password]
		@user = User.find_by_email(email)
		unless @user
			render json: {result: "No user found"}
		else
			if @user.authenticate(password)
				session[:user_id] = @user.id
				render json: {result: "success", name: @user.name, user_id: @user.id, role: @user.role}
			else
				render json: {result: "Passwords do not match"}
			end
		end
	end
	def logout
		session[:user_id] = nil
		render json: {result: 'success'}
	end
	def access
		@user_id = session[:user_id]
		@user = User.find(@user_id)
		if @user.role
			render json: {result: true}
		else
			render json: {result: false}
		end
	end
	def fblogin
		name = params[:name]
		email = params[:email]
		@user = User.find_by_email(email)
		if @user
			session[:user_id] = @user.id
			render json: {result: "success", name: @user.name, user_id: @user.id, role: @user.role}
		else
			@user = User.new
			@user.name = name
			@user.email = email
			@user.password = SecureRandom.hex(6)
			#Send user email about his new password
			if @user.save
				session[:user_id] = @user.id
				render json: {result: "success", name: @user.name, user_id: @user.id, role: @user.role}
			else
				render json: {result: "Save error"}
			end
		end
	end
end
