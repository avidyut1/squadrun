(function (){
	var app = angular.module('squadrun');
	app.controller('addCtrl', ['$rootScope', '$scope', '$state', '$cookieStore', '$http', 'access', function($rootScope, $scope, $state, $cookieStore, $http, access){
		if(!access){
			$state.go('home');
			return;
		}
		if(!$rootScope.name){
			$state.go('login');
			return
		}
		$scope.addqbtnval = "Add Question";
		$scope.answercount = 4;
		$scope.getNum = function (num){
			return new Array(num);
		}
		$scope.iter = $scope.getNum($scope.answercount);
		$scope.$watch('answercount', function (){
			$scope.iter = $scope.getNum($scope.answercount);
		});
		$scope.addQuestion = function(question){
			$scope.addqbtnval = "Adding ...";
			var answers = [];
			$(".answer").each(function (index){
				var val = $(this).val();
				answers.push(val);
			});
			question.answers = answers;
			$http({url: '/questions', method: 'POST', data: question}).then(function (response){
				if(response.data.result == "success"){
					alert("Added question");
					$state.go("home");
				}
				else{
					alert("Error");
				}
				$scope.addqbtnval = "Add Question";
			});
		}
	}])
})();