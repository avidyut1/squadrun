(function (){
	var app = angular.module('squadrun');
	app.controller('mainCtrl', ['$rootScope', 'ngProgressLite', '$cookieStore', 'facebookService', '$scope', "$http", "$state", function($rootScope, ngProgressLite, $cookieStore, facebookService, $scope, $http, $state){
		$scope.fblogin = function (){
			facebookService.login().then(function(token){
      	facebookService.getUserInfo().then(function (response){
        	if(response.email == null){
        		alert("Email not fetched from Facebook");
        		return;
	        }
	        var data = {email : response.email, name : response.name};
	        // console.log(data);
	        $http({"url" : '/fblogin', method: 'POST', data: data}).then(function (response) {
	        	if(response.data.result === "success"){
	        		$rootScope.name = response.data.name;
	        		$rootScope.user_id = response.data.user_id;
	        		$rootScope.role = response.data.role;
		        	$cookieStore.put("name", $rootScope.name);
		        	$cookieStore.put("user_id", $rootScope.user_id);
		        	$cookieStore.put("role", $rootScope.role);
		        	$state.go("home");	
	        	}
	        	else{
	        		alert(response.data.result);
	        	}
	        }); 
	      })
			});
		};
		var name = $cookieStore.get('name');
		var user_id = $cookieStore.get('user_id');
		var role = $cookieStore.get('role');
		if(name){
			$rootScope.name = name;
			$rootScope.user_id = user_id;
			$rootScope.role = role;
		}
		$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams){
			ngProgressLite.start();
			return
		});
		$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams){
      ngProgressLite.done();
      return
    });
	}])
})();