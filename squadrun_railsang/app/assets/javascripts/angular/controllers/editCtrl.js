(function (){
	var app = angular.module('squadrun');
	app.controller('editCtrl', ['$rootScope', '$scope', '$state', '$cookieStore', 'myquestions', '$http', function($rootScope, $scope, $state, $cookieStore, myquestions, $http){
		if(!$rootScope.name){
			$state.go("login");
			return;
		}
		$scope.answercount = 0;
		$scope.questions = myquestions;
		$scope.selectedq = null;
		$scope.editbtnval = "Edit Question";
		$scope.update = function (question){
			$scope.selectedq = question;
			$scope.answercount = question.answer.length;
			$("#modal").foundation("reveal", 'open');
			$scope.iter = $scope.getNum($scope.answercount - $scope.selectedq.answer.length);
			$scope.$watch('answercount', function (){
				$scope.iter = $scope.getNum($scope.answercount - $scope.selectedq.answer.length);
			});
		};
		$scope.getNum = function (num){
			if(num < 0){
				return [];
			}
			return new Array(num);
		}	
		$scope.edit = function (question){
			$scope.editbtnval = "Updating ...";
			var answer = []
			$(".answer").each(function (index){
				var val = $(this).val();
				answer.push({img_url: val});
			});
			question.answer = answer;
			$scope.selectedq = question;
			$scope.answercount = answer.length;
			$http({url: '/questions/'+question.id, method: "PATCH", data: question}).then(function (response){
				if(response.data.result == "success"){
					alert("Edited");
				}
				else{
					alert("Error");
				}
				$scope.editbtnval = "Edit";
			});
		};
		$scope.delete = function (question){
			$http.delete('/questions/'+question.id).then(function (response){
				if(response.data.result == "success"){
					alert("Deleted");
					var ind = $scope.questions.indexOf(question);
					$scope.questions.splice(ind, 1);
				}
				else{
					alert("Error");
				}
			});
		}
	}])
})();