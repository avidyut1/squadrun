(function (){
	var app = angular.module('squadrun');
	app.controller('loginCtrl', ['$rootScope', '$scope', '$state', '$cookieStore', '$http', 'facebookService', function($rootScope, $scope, $state, $cookieStore, $http, facebookService){
		$scope.loginbtnval = "Login";
	  $scope.login = function (user){
	  	$http({url: '/login', method: 'POST', data: user}).then(function (response){
	  		if(response.data.result === "success"){
	  			$rootScope.user_id = response.data.user_id;
	  			$rootScope.name = response.data.name;
	  			$rootScope.role = response.data.role;
	  			$cookieStore.put("name", $rootScope.name);
	  			$cookieStore.put("user_id", $rootScope.user_id);
	  			$cookieStore.put("role", $rootScope.role);
	  			$state.go("home");
	  		}
	  		else{
	  			alert(response.data.result);
	  		}
	  	});
	  };
	}])
})();