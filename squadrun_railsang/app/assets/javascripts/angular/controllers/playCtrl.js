(function (){
	var app = angular.module('squadrun');
	app.controller('playCtrl', ['$rootScope', '$scope', '$state', '$cookieStore', '$http', 'socket', '$timeout', function($rootScope, $scope, $state, $cookieStore, $http, socket, $timeout){
		if(!$rootScope.name){
			$state.go('login');
			return;
		}
		$scope.totalquestions = 0;
		$http.get("/conf").then(function (response){
			$scope.totalquestions = response.data["num_rounds"] * response.data["num_questions"];
		});
		$scope.starttime = null;
		$scope.seconds = 0;
		$scope.minutes = 0;
		$scope.hours = 0;
		$scope.answertime = [];
		$scope.questioncounter = -1;
		var gettimediff = function (time1, time2){
			var diff = (time2 - time1);
			var msec = diff;
	    var hh = Math.floor(msec / 1000 / 60 / 60);
	    msec -= hh * 1000 * 60 * 60;
	    var mm = Math.floor(msec / 1000 / 60);
	    msec -= mm * 1000 * 60;
	    var ss = Math.floor(msec / 1000);
	    msec -= ss * 1000;
	    return [hh, mm, ss];
		}
		var setTimer = function(){
			var res = gettimediff($scope.starttime, new Date());
	    $scope.seconds = res[2];
	    $scope.minutes = res[1];
	    $scope.hours = res[0];
	    $timeout(setTimer, 1000);
		}
		$scope.questions = null;
		$scope.show = false;
		$scope.showind = -1;
		$scope.selectedq = null;
		$scope.answer = null;
		$scope.gameid = null;
		$scope.round = 1;
		$scope.question = null;
		$scope.cstatus = null;
		socket.emit("message", {user_id: $rootScope.user_id}, setStatusConnected());
		function setStatusConnected(){
			$scope.cstatus = "Connected";
			socket.emit("userfree", {"user_id": $rootScope.user_id}, function (){});
			$timeout(function () {
				$scope.cstatus = "Finding a player to play ..";
				pair();
			}, 1000);
		};
		function pair(){
			socket.emit("pair", {user_id: $rootScope.user_id}, function (){});
		}
		function gameEnd(){
			alert("game ends");
			$state.go("home");
		}
		$scope.sendAnswer = function (){
			if($scope.answer == null)
				return;
			var time = new Date();
			$scope.answertime.push([$scope.showind, $scope.answer, gettimediff($scope.starttime, time)]);
			socket.emit("answer", {user_id: $rootScope.user_id, answer: $scope.answer, question: $scope.selectedq.id, time: time.getTime()}, function(){});
		};
		socket.on("message", function(data){
			if(data == "Waiting for a player"){
				$timeout(pair, 2000);
			}
			else{
				var index = data.indexOf("Paired");
				var disind = data.indexOf("disconnected");
				var openind = data.indexOf("open");
				var playind = data.indexOf("Playing");
				var mismatch = data.indexOf("mismatch");
				var cg = data.indexOf("close");
				if(index >= 0){
					var usersec = data.split(" ")[2];
					$http({url: '/games', method: 'POST', data: {"first_user": $rootScope.user_id, "second_user": usersec}}).then(function (response){
						if(response.data.result == "success"){
							$scope.gameid = response.data.gameid;
							socket.emit("game", {fu: $rootScope.user_id, su: usersec, gid: $scope.gameid}, function(){});				
						}
						else{
							alert("Error");
						}
					});	
					$scope.cstatus = data;
				}
				else if(disind >= 0){
					$state.go("play");
					$scope.cstatus = data;
				}
				else if(openind >= 0){
					var data = data.split(" ");
					if(data.length == 3){
						if(data[1]){
							$scope.gameid = data[1];
							$scope.showind = data[2];
							$scope.questioncounter = 0;
							console.log($scope.showind);
						}
					}
					else if(data.length == 2){
						$scope.showind = data[1];
					}
					$http.get('/questions/'+$scope.showind).then(function(response){
						$scope.question = response.data;
						$scope.selectedq = $scope.question;
						$scope.show = true;	
					});
					if($scope.questioncounter == 0){
						$scope.starttime = new Date();
						setTimer();
					}
					$scope.questioncounter+=1;
					$scope.round = (($scope.questioncounter + 1) / 3) + 1;
					// if($scope.showind >= 1){
						// alert("Answer matched");
					// }
					// var data = {gameid: $scope.gameid, questionid: $scope.showind, answerid: $sco.answer[$scope.answer-1].id};
					// $http({url: '/gamedetails', method: 'POST', data: data}).then(function (response){
						// if(response.data.result == "success"){
							// console.log("created game details");
						// }
						// else{
							// alert("save error");
						// }
					// });
					if($scope.questioncounter >= $scope.totalquestions+1){
						// console.log("allquestions done");
						socket.emit("gameend", {"fu": $rootScope.user_id}, function(){});
					}
				}
				else if(playind >= 0){
					$scope.showind = -1;
					$scope.cstatus = data;
				}
				else if(mismatch >= 0){
					alert("Answer mismatch");
				}
				else if(cg >= 0){
					console.log("close game");
					gameEnd();
				}
				else{
					$scope.cstatus = data;
				}
			}
			// console.log(data);
		})
	}])
})();