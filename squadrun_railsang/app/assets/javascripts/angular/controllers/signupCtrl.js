(function (){
	var app = angular.module('squadrun');
	app.controller('signupCtrl', ['$http', '$cookieStore', '$rootScope', '$state', '$scope', 'facebookService', function($http, $cookieStore, $rootScope, $state, $scope, facebookService){
		$scope.signupbtnval = "Sign Up";
		$scope.signup = function (user){
			$http({url: '/signup', method: 'POST', data: user}).then(function(response){
				if(response.data.result === "success"){
					$rootScope.name = user.name;
					$rootScope.user_id = response.data.user_id;
					$cookieStore.put("name", $rootScope.name);
					$cookieStore.put("user_id", $rootScope.user_id);
					$state.go("home");
				}
				else{
					alert(response.data.result);
				}
			});
		};
	}]);
})();