(function (){
	var app = angular.module('squadrun');
	app.controller('homeCtrl', ['$rootScope', '$scope', '$state', '$http', function($rootScope, $scope, $state, $http){
		if(!$rootScope.name){
			$state.go('login');
			return;
		}
		$scope.gamewp = null;
		$scope.gamelp = null;
		$scope.roundwp = null;
		$scope.roundlp = null;
		$scope.questions = null;
		$scope.rounds = null;
		$http.get('/conf').then(function (response){
			$scope.gamewp = response.data["game_win_points"]
			$scope.gamelp = response.data["game_loose_points"]
			$scope.roundwp = response.data["round_win_points"]
			$scope.roundlp = response.data["round_loose_points"]
			$scope.questions = response.data["num_questions"]
			$scope.rounds = response.data["num_rounds"]
		});
		$scope.updateconfig = function (){
			// console.log($scope.roundlp)
			var val = $("#formc :input");
			// for (var i = val.length - 1; i >= 0; i--) {
				// console.log(val[i].value)
			// };
			$http({url: '/conf', method: "POST", data:{"gamewp":val[2].value, "gamelp":val[3].value, "roundlp":val[5].value, "roundwp":val[4].value,"questions":val[1].value,"rounds":val[0].value}}).then(function (response){
				if(response.data.result == "success")
					alert("Updated")
			})
		}
	}])
})();