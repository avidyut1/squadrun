(function (){
	var app = angular.module('squadrun');
	app.controller('logoutCtrl', ['$http', '$state', '$rootScope', '$cookieStore', 'facebookService', function($http, $state, $rootScope, $cookieStore, facebookService){
		$http.get('/logout').then(function(response){
			if(response.data.result === "success"){
				$cookieStore.remove('name');
				$rootScope.name = null;
				facebookService.logout();
				$state.go("login");
			}
		});
	}])
})();