(function (){
	var app = angular.module('squadrun');
	app.directive('homeNav', function(){
		return {
			restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
			templateUrl: 'home_nav.html',
		};
	});
})();