(function (){
	var mod = angular.module('socketService',[]);
	mod.service('socket', ['$rootScope', function($rootScope){
		var socket = io.connect('http://vagrant.com:7000/',{'reconnection' : true});
			this.on = function (eventName, callback){
				socket.on(eventName, function (){
					var args = arguments;
					$rootScope.$apply(function (){
						callback.apply(socket, args);
					});
				});
			};
			this.emit = function(eventName, data, callback){
				socket.emit(eventName, data, function(){
					var args = arguments;
					$rootScope.$apply(function(){
						if(callback){
							callback.apply(socket, args);
						}
					})
				});
			}
		}
	]);
})();
