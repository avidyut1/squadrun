(function (){
	var mod = angular.module('facebookservice', []);
	mod.service('facebookService', ['$q', function($q){
		this.login = function(){
			var deferred = $q.defer();
			FB.login(function (response){
				if(response.authResponse){
					deferred.resolve(response.authResponse.accessToken)
				}
				else{
					deferred.reject('Error occured');
				}
			});
			return deferred.promise;
		};
		this.getLoginStatus = function (){
			var deferred = $q.defer();
			FB.getLoginStatus(function(response){
				if(response.status == "connected"){
					deferred.resolve("Logged in");
				}
				else if(response.status == "not_authorized"){
					deferred.resolve("Not authorized");
				}
				else{
					deferred.resolve("Not logged in");
				}
			});
			return deferred.promise;
		};
		this.getUserInfo = function (){
			var deferred = $q.defer();
			FB.api('/me',{
				fields: ['email', 'name']
			},function (response){
				if(!response || response.error){
					deferred.reject('Error occured');
				}
				else{
					deferred.resolve(response);
				}
			});
			return deferred.promise;
		};
		this.logout = function (){
			var deferred = $q.defer();
			FB.logout(function(response){
				deferred.resolve(response);
			})
			return deferred.promise;
		};
	}]);
})();
