(function (){
	var app = angular.module('squadrun', ['templates', 'ui.router', 'ngProgressLite', 'facebookservice', 'ngCookies', 'socketService']);
	app.config(['$httpProvider', '$stateProvider', '$urlRouterProvider', function($httpProvider, $stateProvider, $urlRouterProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
	}]);
	app.run(['$rootScope', '$window', function($rootScope, $window) {
  	$rootScope.name = null;
	  $window.fbAsyncInit = function() {
	    FB.init({ 
      	appId: '410762342442694', 
      	channelUrl: 'app/channel.html', 
      	status: true, 
      	cookie: true, 
      	xfbml: true 
	    });
  	};
  	(function(d){
    	var js, 
    	id = 'facebook-jssdk', 
    	ref = d.getElementsByTagName('script')[0];
    	if (d.getElementById(id)) {
     		return;
    	}
    	js = d.createElement('script'); 
    	js.id = id; 
    	js.async = true;
    	js.src = "//connect.facebook.net/en_US/all.js";
    	ref.parentNode.insertBefore(js, ref);
  	}(document));
	}]);
	app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/home');
		$stateProvider.state('home',{
			url: '/home',
			templateUrl: 'home.html',
			controller: 'homeCtrl'
		});
		$stateProvider.state('play',{
			url: '/play',
			templateUrl: 'play.html',
			controller: 'playCtrl'
		});
		$stateProvider.state('add',{
			url: '/add',
			templateUrl: 'add.html',
			controller: 'addCtrl',
			resolve: {
				access: ["$http", function ($http){
					return $http.get('/access').then(function (response){
						return response.data.result;
					});
				}]
			}
		});
		$stateProvider.state('edit',{
			url: '/edit',
			templateUrl: 'edit.html',
			controller: 'editCtrl',
			resolve : {
				myquestions: ['$http', function($http){
					return $http.get("/userquestions").then(function (response){
						return response.data.questions;
					}, function (error){
						return "Error";
					});
				}]
			}
		});
		$stateProvider.state('login',{
			url: '/login',
			templateUrl: 'login.html',
			controller: 'loginCtrl'
		});
		$stateProvider.state('hof',{
			url: '/hof',
			templateUrl: 'hof.html',
			controller: 'hofCtrl',
			resolve : {
				users: ['$http', function($http){
					return $http.get("/hof").then(function (response){
						return response.data;
					})
				}]
			}
		});
		$stateProvider.state('logout',{
			url: '/logout',
			controller: 'logoutCtrl',
		});
		$stateProvider.state('signup',{
			url: '/signup',
			templateUrl: 'signup.html',
			controller: 'signupCtrl',
		});
	}])
})();