# Set the working application directory
# working_directory "/path/to/your/app"
working_directory "/var/www/Sandbox/squadrun/squadrun_railsang/"

# Unicorn PID file location
#pid "/var/assembly/instajobrailsang/pids/unicorn.pid"

# Path to logs
stderr_path "/var/www/Sandbox/squadrun/squadrun_railsang/log/unicorn.log"
stdout_path "/var/www/Sandbox/squadrun/squadrun_railsang/log/unicorn.log"

# Unicorn socket
listen "/tmp/unicorn.squadrun.sock"

# Number of processes
# worker_processes 4
worker_processes 2

# Time-out
timeout 30

preload_app true

# If using ActiveRecord, disconnect (from the database) before forking.
before_fork do |server, worker|
  defined?(ActiveRecord::Base) &&
    ActiveRecord::Base.connection.disconnect!
end

# After forking, restore your ActiveRecord connection.
after_fork do |server, worker|
  defined?(ActiveRecord::Base) &&
    ActiveRecord::Base.establish_connection
end
