Rails.application.routes.draw do
  root "home#index"
  match "/fblogin" => "home#fblogin", via: [:get, :post]
  match "/login" => "home#login", via: :post
  match "/logout" => "home#logout", via: [:get, :post]
  match "/signup" => "home#signup", via: [:get, :post]
  match "/userquestions" => "questions#userquestions", via: :get
  match "/access" => "home#access", via: :get
  match "/hof" => "home#hof", via: :get
  match "/conf" => "home#conf", via: [:get, :post] 
  resources :questions
  resources :answers
  resources :games
  resources :gamedetails
end
