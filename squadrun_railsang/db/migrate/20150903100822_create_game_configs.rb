class CreateGameConfigs < ActiveRecord::Migration
  def change
    create_table :game_configs do |t|
			t.integer :num_rounds
    	t.integer :num_questions
    	t.integer :round_win_points
    	t.integer :round_loose_points
    	t.integer :game_win_points
    	t.integer :game_loose_points
      t.timestamps null: false
    end
  end
end
