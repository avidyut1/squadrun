class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
    	t.string :title
    	t.string :img_url
    	t.integer :user_id
      t.timestamps null: false
    end
  end
end
