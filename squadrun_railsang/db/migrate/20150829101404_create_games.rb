class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
    	t.integer :first_user_id
    	t.integer :second_user_id
      t.timestamps null: false
    end
  end
end
