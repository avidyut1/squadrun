# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150903100822) do

  create_table "answers", force: :cascade do |t|
    t.string   "img_url",     limit: 255
    t.integer  "question_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "game_configs", force: :cascade do |t|
    t.integer  "num_rounds",         limit: 4
    t.integer  "num_questions",      limit: 4
    t.integer  "round_win_points",   limit: 4
    t.integer  "round_loose_points", limit: 4
    t.integer  "game_win_points",    limit: 4
    t.integer  "game_loose_points",  limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "game_details", force: :cascade do |t|
    t.integer  "game_id",     limit: 4
    t.integer  "question_id", limit: 4
    t.integer  "answer_id",   limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "games", force: :cascade do |t|
    t.integer  "first_user_id",  limit: 4
    t.integer  "second_user_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "questions", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.string   "img_url",    limit: 255
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",           limit: 255
    t.string   "name",            limit: 255
    t.string   "password_digest", limit: 255
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "score",           limit: 4
    t.boolean  "role",                        default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", using: :btree

end
